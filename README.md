Hi,

Please find below details to test the API's

1) Insert the player info, Please use below url

URL: http://localhost:8083/oyo-Hotel/insertplayer
Request: Content-Type : application/json
{
	"playerName":"player3",
	"score":1450,
	"dateTime":"{{CurrentDatetime}}"
}

Response: player record updated successfully com.oyoHotels.model.players@26379e74

2) get te player record, please use below URL
URL: http://localhost:8083/oyo-Hotel/getplayer?playerName=player2

response: 

{
    "playerName": "player2",
    "score": 1250,
    "dateTime": "2021-02-16T00:46:33.908Z"
}

3) get te player record, please use below URL

URL: http://localhost:8083/oyo-Hotel/deleteinfo?playerName=player3

Response: record deleted successfully...!!

4) Get the player info after the given date 

URL: http://localhost:8083/oyo-Hotel/playerInfoAfterDate/2020-11-01

Response:

[
    {
        "playerName": "player1",
        "score": 950,
        "dateTime": "2021-02-16T00:46:16.565Z"
    },
    {
        "playerName": "player2",
        "score": 1250,
        "dateTime": "2021-02-16T00:46:33.908Z"
    }
]

5) Get the player info before the given date 

URL: http://localhost:8083/oyo-Hotel/playerInfoBeforeDate/2021-03-01

Response:

[
    {
        "playerName": "player1",
        "score": 950,
        "dateTime": "2021-02-16T00:46:16.565Z"
    },
    {
        "playerName": "player2",
        "score": 1250,
        "dateTime": "2021-02-16T00:46:33.908Z"
    }
]

6) Get the player info between the given dates 

URL: http://localhost:8083/oyo-Hotel/playerInfoBetweenDates/2021-01-01,2021-03-01

Response:

[
    {
        "playerName": "player1",
        "score": 950,
        "dateTime": "2021-02-16T00:46:16.565Z"
    },
    {
        "playerName": "player2",
        "score": 1250,
        "dateTime": "2021-02-16T00:46:33.908Z"
    }
]

7) Get the minimum and maximum scores of player.

URL: http://localhost:8083/oyo-Hotel/MaxMinScore/player1

Response: MAX_SCORE: 950 MIN_SCORE: 400

Note: This score details getting from two tables are current player details and playerHist table.


