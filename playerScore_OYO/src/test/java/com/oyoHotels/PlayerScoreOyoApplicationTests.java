package com.oyoHotels;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

import com.oyoHotels.model.players;

@SpringBootTest
class PlayerScoreOyoApplicationTests {
	

	@Autowired
    private TestRestTemplate restTemplate;
	
	@LocalServerPort
    private int port;
	
	private String getRootUrl() {
        return "http://localhost:" + port +"/oyo-Hotel/";
    }


	@Test
	public void getplayerInfoAfterDateTest() {
		players player = restTemplate.getForObject(getRootUrl()+"/playerInfoAfterDate/2021-01-01", players.class);
		System.out.println("player name "+player.getPlayerName()+" score "+player.getScore());
		assertNotNull(player);
	}
	
	@Test
	public void getMaxMinScoreTest() {
		String[] playerScore = restTemplate.getForObject(getRootUrl()+"/MaxMinScore/player1", String[].class);
		
		System.out.println("MAX_SCORE: "+playerScore[0]+"~ MIN_SCORE: "+playerScore[1]);
		assertNotNull(playerScore);	
	}

}
