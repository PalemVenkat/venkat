package com.oyoHotels.controller;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.oyoHotels.model.players;
import com.oyoHotels.services.playerServices;

@RestController
public class playerScoreController {
	
	@Autowired
	playerServices playerServices;
	
	@GetMapping(path="/playerinfo")
	public List<players> getAllPlayerInformation() {
		
		return playerServices.getplayerDetails();
	}
	
	@PostMapping(path = "/insertplayer")
	public String insertOrUpdate(@RequestBody players player) 
	{
		playerServices.insertPlayers(player);
		return " player record updated successfully "+player.toString();
		
	}
	
	
	
	
//........................................................................................................................................................//
	
	
	
	@GetMapping(path="/deleteinfo")
	public String deletePlayers(@RequestParam(value = "playerName") String playerName) 
	{
		playerServices.deletePlayers(playerName);
		return "record deleted successfully...!!";
	}
	
	@GetMapping(path="/getplayer")
	public players getPlayerInfo(@RequestParam(value="playerName") String playerName)
	{
		return playerServices.getInformation(playerName);
		
	}
	@RequestMapping(path="/playerInfoAfterDate/{date}")
	@ResponseBody
	public List<players> getplayerInfoAfterDate(@PathVariable String date) {
		
		return playerServices.getplayerInfoAfterDate(date);
	}
	@RequestMapping(path="/playerInfoBeforeDate/{date}")
	@ResponseBody
	public List<players> getplayerInfoBeforeDate(@PathVariable String date) {
		
		return playerServices.getplayerInfoBeforeDate(date);
	}
	
	@RequestMapping(path="/playerInfoBetweenDates/{date1},{date2}")
	@ResponseBody
	public List<players> getplayerInfoBetweenDates(@PathVariable String date1, @PathVariable String date2) {
		
		return playerServices.getplayerInfoBetweenDates(date1, date2);
	}
	@RequestMapping(path="/MaxMinScore/{player}")
	@ResponseBody
	public String getMaxMinScore(@PathVariable String player) {
		
		String[] strArray = playerServices.getMaxMinScore(player).split("[,]",0);
		return "MAX_SCORE: "+strArray[0]+"~ MIN_SCORE: "+strArray[1];
	}

}
