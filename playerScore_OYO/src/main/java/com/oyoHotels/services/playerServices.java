package com.oyoHotels.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.oyoHotels.model.players;
import com.oyoHotels.model.playersHist;
import com.oyoHotels.repository.playerRepository;

@Service
public class playerServices {
	
	@Autowired
	playerRepository playerRepo;
	
	
	
	public void insertOrUpdatePlayerRecords(players player, String playerName) {
		playerRepo.save(player);
	}
	
	public List<players> getplayerDetails() {
		
		return playerRepo.findAll();
		
	}
	
	public players getInformation(String player_name) {
		return playerRepo.findById(player_name).get();
		
	}
	
	public void deletePlayers(String player_name) {
		 playerRepo.deleteById(player_name);
		
	}

	public void insertPlayers(players player) {
		playerRepo.save(player);
		
	}
	
	public List<players> getplayerInfoAfterDate(String date) 
	{
		List<players> playersinfo = playerRepo.getplayerInfoAfterDate(date);
		
		return playersinfo;
		
	}
	public List<players> getplayerInfoBeforeDate(String date) 
	{
		List<players> playersinfo = playerRepo.getplayerInfoBeforeDate(date);
		
		return playersinfo;
		
	}
	
	public List<players> getplayerInfoBetweenDates(String date1, String date2) 
	{
		System.out.println("date "+date1+" date2 "+date2);
		List<players> playersinfo = playerRepo.getplayerInfoBetweenDates(date1, date2);
		
		return playersinfo;
		
	}
	
	public String getMaxMinScore(String player) 
	{
		String player1 = player;
		String playersinfo = playerRepo.getMaxMinScore(player, player1);
		return playersinfo;
		
	}
	
}
