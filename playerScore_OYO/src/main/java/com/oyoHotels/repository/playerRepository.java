package com.oyoHotels.repository;

import java.util.List;

import javax.persistence.NamedQuery;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.oyoHotels.model.players;
import com.oyoHotels.model.players;

@Repository
public interface playerRepository extends JpaRepository<players, String>{
	
	@Query(value = "select * from players where date_time > ?", nativeQuery = true)
	public List<players> getplayerInfoAfterDate(String date);
	
	@Query(value = "select * from players where date_time < ?", nativeQuery = true)
	public List<players> getplayerInfoBeforeDate(String date);
	
	@Query(value = "select * from players where date_time between ? and ?", nativeQuery = true)
	public List<players> getplayerInfoBetweenDates(String date1, String date2);
	
	@Query(value = "select MAX(max_score) as max_score, MIN(min_score) as min_score from (select MAX(score) as max_score, MIN(score) as min_score from players where player_name = ? UNION ALL select MAX(score) as max_score, MIN(score) as min_score from playershist where player_name = ?) as subquey", nativeQuery = true)
	public String getMaxMinScore(String player, String player1);
}
