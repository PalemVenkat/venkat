package com.oyoHotels;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PlayerScoreOyoApplication {

	public static void main(String[] args) {
		SpringApplication.run(PlayerScoreOyoApplication.class, args);
	}

}
